//###################################Game#######################################

function Game(boardRow) {
  this.menu = new Menu();
  this.board = new Board(boardRow, this);
  this.graph = new Graph(this.board);
  this.board.setupFindingPath(this.graph);

  //console.log(this.board);
  //console.log(this.board.preview);
  //console.log(this.graph);

  this.board.addBubblesAtStart(3);
  this.board.preview.addBubbles();


  let addPointsAccordingToBeatenBubbles = (amount) => {
    let counter = 1;
    let value = amount * counter;
    this.menu.stateSection.points.addPoints(value);
  };

  let checkIfGameEnded = () => {

    let isBoardFull = () => {
      for (let row of this.board.cells) {
        for (let cell of row) {
          if (cell.bubble === null) return false;
        }
      }
      return true;
    };

    let isBoardEmpty = () => {
      for (let row of this.board.cells) {
        for (let cell of row) {
          if (cell.bubble !== null) return false;
        }
      }
      return true;
    };

    if (isBoardFull()) {
      this.menu.stateSection.newGame.htmlElement.style.backgroundColor = "tomato";
      this.menu.stateSection.points.htmlElement.style.color = "red";
      this.menu.stateSection.newGame.alertGameEnd = true;
    } else if (isBoardEmpty()) {
      this.menu.stateSection.newGame.htmlElement.style.backgroundColor = "tomato";
      this.menu.stateSection.points.htmlElement.style.color = "red";
      this.menu.stateSection.newGame.alertGameEnd = true;
    }
  };

  this.notifyMovement = function(endOfPathCell) {
    //is false or is array of cells with bubbles to beat
    let cellsWithBubblesToBeat = this.board.checkIfToBeatBubbles(endOfPathCell);
    checkIfGameEnded();

    if (cellsWithBubblesToBeat) {
      this.board.beatBubbles(cellsWithBubblesToBeat);
      addPointsAccordingToBeatenBubbles(cellsWithBubblesToBeat.length);
      checkIfGameEnded();

    } else {

      let addedBubblesCells = this.board.addBubblesFromPreview();
      this.board.preview.addBubbles();
      let cellsWithBubblesToBeat = this.board.checkIfToBeatBubbles(addedBubblesCells);
      checkIfGameEnded();

      if (cellsWithBubblesToBeat) {
        this.board.beatBubbles(cellsWithBubblesToBeat);
        addPointsAccordingToBeatenBubbles(cellsWithBubblesToBeat.length);
        checkIfGameEnded();
      }
    }

  };

}

//###################################Menu#######################################

function Menu() {
  this.htmlElement = createHtmlElement("menuDiv");
  this.titleSection = new TitleSection();
  this.stateSection = new StateSection();
}

function TitleSection() { //todo: create separate add text div function
  let menuDiv = document.getElementsByClassName("menuDiv")[0];
  this.htmlElement = createHtmlElement("titleSectionDiv", menuDiv);

  let text = document.createTextNode("~ bubble game ~");
  this.textParagraph = document.createElement('p');
  this.textParagraph.appendChild(text);
  this.htmlElement.appendChild(this.textParagraph);
}

function StateSection() {
  let menuDiv = document.getElementsByClassName("menuDiv")[0];
  this.htmlElement = createHtmlElement("stateSectionDiv", menuDiv);

  this.points = new Points();
  this.newGame = new NewGame();
}

function Points() {
  let stateSectionDiv = document.getElementsByClassName("stateSectionDiv")[0];
  this.htmlElement = createHtmlElement("pointsDiv", stateSectionDiv);

  this.value = 0;

  let text = document.createTextNode("points: " + this.value);
  let textParagraph = document.createElement('p');
  textParagraph.appendChild(text);
  this.htmlElement.appendChild(textParagraph);

  this.addPoints = (n) => {
    this.value += n;
    this.updateHtml();
  };

  this.updateHtml = () => {
    var element = this.htmlElement;
    while (element.firstChild) element.removeChild(element.firstChild);

    let text = document.createTextNode("points: " + this.value);
    let textParagraph = document.createElement('p');
    textParagraph.appendChild(text);
    element.appendChild(textParagraph);
  };
}

function NewGame() {
  let stateSectionDiv = document.getElementsByClassName("stateSectionDiv")[0];
  this.htmlElement = createHtmlElement("newGameDiv", stateSectionDiv);
  this.alertGameEnd = false;

  let text = document.createTextNode("new game");
  this.textParagraph = document.createElement('p');
  this.textParagraph.appendChild(text);
  this.htmlElement.appendChild(this.textParagraph);

  this.htmlElement.addEventListener("click", () => {
    var element = document.body;
    while (element.firstChild) {
      element.removeChild(element.firstChild);
    }

    init.clearAll();
    init.boardRow = 9;
    init.game = new Game(init.boardRow);
  });

  this.htmlElement.addEventListener("mouseover", () => {
    if (!this.alertGameEnd) this.htmlElement.style.backgroundColor = "lightgray";
  });

  this.htmlElement.addEventListener("mouseout", () => {
    if (!this.alertGameEnd) this.htmlElement.style.backgroundColor = "white";
  });
}

//###################################Board######################################

function Board(boardRow, observer) {
  this.htmlElement = createHtmlElement("board");

  this.width = boardRow;
  this.height = boardRow;
  this.observer = observer;
  this.startOfPath = null;
  this.endOfPath = null;
  this.path = null;
  this.enablePathPreview = false;


  //creates cells 2-dim array with html elements
  let createCellsArray = (width, height, board) => {
    let boardDiv = document.getElementsByClassName("board")[0];
    let cells = [height];

    for (var i = 0; i < height; i++) {
      cells[i] = [width];
      for (var j = 0; j < width; j++) {
        cells[i][j] = new Cell(createHtmlElement("cell", boardDiv), board);
      }
    }
    return cells;
  };

  this.cells = createCellsArray(this.width, this.height, this);


  this.preview = new Preview(3);

  this.setupFindingPath = function(graph) {
    this.graph = graph;
  };


  //returns array of cells adjacent to given cell
  this.getAdjacentCellsOfCell = function(cell) {
    for (var y = 0; y < this.cells.length; y++)
      for (var x = 0; x < this.cells[y].length; x++)
        if (this.cells[y][x] === cell)
          return getAdjacentCellsByPositionInArray(x, y);
  };

  let getAdjacentCellsByPositionInArray = (x, y) => {
    let neighbours = [];
    let adjacentDirections = [
      [0, 1],
      [0, -1],
      [1, 0],
      [-1, 0]
    ];

    for (let direction of adjacentDirections) {
      let neighbourX = direction[0] + x;
      let neighbourY = direction[1] + y;
      if ((neighbourX >= 0 & neighbourY >= 0) & (neighbourX <= (this.width - 1) & neighbourY <= (this.height - 1))) {
        neighbours.push(this.cells[neighbourY][neighbourX]);
      }
    }
    return neighbours;
  };


  this.addBubblesAtStart = function(n) { //attach to below funcion (?)
    for (let i = 0; i < n; i++) {

      let emptyCells = getEmptyCellsArray(this.cells);
      let cell = getRandomIndexFromArray(emptyCells);
      let bubble = new Bubble(cell.htmlElement);

      cell.pushBubble(bubble);

    }
  };

  this.addBubblesFromPreview = function() {
    let cells = [];
    let bubbles = this.preview.getBubbles();
    this.preview.removeBubbles();

    for (let bubble of bubbles) {

      let emptyCells = getEmptyCellsArray(this.cells);
      if (emptyCells.length === 0) continue;
      let cell = getRandomIndexFromArray(emptyCells);
      cells.push(cell);

      cell.pushBubble(bubble);

    }
    return cells;
  };

  let getEmptyCellsArray = (cellsArray) => {
    var emptyCells = [];
    for (let row of cellsArray) {
      for (let item of row) {
        if (item.bubble === null) {
          emptyCells.push(item);
        }
      }
    }
    return emptyCells;
  };

  //returns random index value form given array
  let getRandomIndexFromArray = (array) => {
    return array[Math.floor(Math.random() * array.length)];
  };


  this.updateCellOnClick = function(cell) {
    if (cell.bubble) {

      if (!isCellSurrounded(cell)) {

        if (this.startOfPath === null) {
          selectStartPath(cell);
          this.enablePathPreview = true;
          //console.log(this.startOfPath, "start");
        } else {

          if (this.startOfPath === cell) {
            unselectStartPath();
            this.enablePathPreview = false;
            //console.log(this.startOfPath);
          } else {
            unanimateOnSelect(this.startOfPath.bubble.htmlElement, this.startOfPath.htmlElement);
            selectStartPath(cell);
            this.enablePathPreview = true;
            //console.log(this.startOfPath, "new start");
          }
        }
      }

    } else {

      if (this.endOfPath === null && this.startOfPath !== null) {

        this.endOfPath = cell;
        this.enablePathPreview = false;
        //console.log(this.endOfPath, "mete");

        if (this.checkoutPath(this.endOfPath)) {
          this.moveBubble(this.checkoutPath(this.endOfPath));
        } else {
          unanimateOnSelect(this.startOfPath.bubble.htmlElement, this.startOfPath.htmlElement);
          clearStartAndEndOfPathVariables();
          //console.log(this.startOfPath, this.endOfPath);
        }

      }
    }
  };

  let isCellSurrounded = (cell) => {
    let neighbours = this.getAdjacentCellsOfCell(cell);

    for (let neighbour of neighbours) {
      if (!neighbour.bubble) return false;
    }
    return true;
  };

  let selectStartPath = (cell) => {
    animateOnSelect(cell.bubble.htmlElement, cell.htmlElement);
    this.startOfPath = cell;
  };

  let unselectStartPath = () => {
    unanimateOnSelect(this.startOfPath.bubble.htmlElement, this.startOfPath.htmlElement);
    this.startOfPath = null;
  };


  this.checkoutPath = function(endOfPath) { //todo: inner function
    let startOfPathID = getIDFromCell(this.startOfPath);
    let endOfPathID = getIDFromCell(endOfPath);
    let cellsWithBubblesIDs = getIDOfCellsWithBubbles();

    let pathInIDs = this.graph.findShortestPath(this.graph.adjacencyList, startOfPathID, endOfPathID, cellsWithBubblesIDs);

    return pathInIDs !== undefined ? this.path = getCellsFromIDs(pathInIDs) : this.path = null;
  };

  let getIDFromCell = (cell) => {
    for (let node of this.graph.nodes) {
      if (node.content === cell) {
        return node.id;
      }
    }
  };

  let getIDOfCellsWithBubbles = () => {
    let cellsWithBubbles = [];
    for (let row of this.cells) {
      for (let cell of row) {

        if (cell === this.startOfPath) {
          continue;
        }

        let x = getIDFromCell(cell);
        if (cell.bubble) {
          cellsWithBubbles[x] = true;
        } else {
          cellsWithBubbles[x] = false;
        }

      }
    }
    return cellsWithBubbles;
  };


  this.moveBubble = function(pathInIDs) {
    //console.log(this.path);
    unanimateOnSelect(this.startOfPath.bubble.htmlElement, this.startOfPath.htmlElement);
    var bubble = this.startOfPath.bubble;
    //setTimeout(function(bubble) {
    this.startOfPath.removeBubble(bubble);
    //}, 30);

    disableCellEvents();
    setIntervalOnBubbleMove(this.path, bubble);
    // setIntervalOnBubbleMove(path, bubble).then(clearStartAndEndOfPathVariables).then(enableCellEvents).catch(function(err) {
    //   console.log(err);
    // });

  }; //todo: inner method; make with Promise and timeout

  let getCellsFromIDs = (array) => {
    let path = [];

    for (let id of array) {
      for (let node of this.graph.nodes) {
        if (node.id === id) {
          path.push(node.content);
        }
      }
    }

    return path;
  };

  let disableCellEvents = () => {
    for (let row of this.cells) {
      for (let cell of row) {
        cell.htmlElement.style.pointerEvents = 'none';
      }
    }
  };

  let setIntervalOnBubbleMove = (path, bubble) => {
    let i = 0;
    let interval = setInterval(function() {

      if (i === 0) path[i].pushBubble(bubble);
      else {
        path[i - 1].removeBubble(bubble);
        path[i].pushBubble(bubble);
      }

      if (i === path.length - 1) {
        clearInterval(interval);
        setPathAfterMove(path);
      }

      i++;
    }, 100);

  };

  //todo:
  // function setIntervalOnBubbleMove(path, bubble) {
  //   return new Promise(function(resolve, reject) {
  //     let i = 0;
  //     let interval = setInterval(function() {
  //       if (i === 0) path[i].pushBubble(bubble);
  //       else {
  //
  //         path[i - 1].removeBubble(bubble);
  //         path[i].pushBubble(bubble);
  //         if (i === path.length - 1) {

  //           clearInterval(interval);
  //           colorPathAfterMove(path);
  //           resolve("Stuff worked!");
  //         }
  //
  //       }
  //       i++;
  //     }, 130);
  //   });
  // }

  let setPathAfterMove = (path) => {
    colorPath(path);

    setTimeout(() => {
      //console.log("CURRENT", this.startOfPath);
      uncolorPath(path);
      this.observer.notifyMovement(this.endOfPath);
      clearStartAndEndOfPathVariables();
      enableCellEvents();
    }, 400);
  };

  let colorPath = (path) => {
    this.startOfPath.htmlElement.style.backgroundColor = "#e6e6e6";
    for (let cell of path) {
      cell.htmlElement.style.backgroundColor = "#e6e6e6";
    }
  };

  let uncolorPath = (path) => {
    this.startOfPath.htmlElement.style.backgroundColor = "lightgray";
    for (let cell of path) {
      cell.htmlElement.style.backgroundColor = "lightgray";
    }
  };

  let clearStartAndEndOfPathVariables = () => {
    this.startOfPath = null;
    this.endOfPath = null;
    //console.log(this.startOfPath, this.endOfPath, "eee");
  };

  let enableCellEvents = () => {
    for (let row of this.cells) {
      for (let cell of row) {
        cell.htmlElement.style.pointerEvents = 'auto';
      }
    }
  };


  this.updateCellAnimationOnMouseover = function(cell) { //rethink in free time
    if (this.enablePathPreview) {
      if (!cell.bubble) {
        if (this.checkoutPath(cell)) {
          colorPath(this.checkoutPath(cell));
        }
      }
    }
  };

  this.updateCellAnimationOnMouseout = function(cell) { //rethink in free time
    if (this.enablePathPreview) {
      if (!cell.bubble) {
        if (this.checkoutPath(cell)) {
          uncolorPath(this.path);
        }
      }
    }
  };


  this.checkIfToBeatBubbles = function(cells) {
    let cellsToCheck = null; //shorter
    if (Array.isArray(cells)) {
      cellsToCheck = cells;
    } else {
      cellsToCheck = [cells];
    }

    let cellsWithBubblesToBeat = [];
    let directions = { //shorter(?)
      horizontal: [
        [0, 1],
        [0, -1]
      ],
      vertical: [
        [-1, 0],
        [1, 0]
      ],
      diagonal_1: [
        [1, 1],
        [-1, -1]
      ],
      diagonal_2: [
        [-1, 1],
        [1, -1]
      ]
    };

    for (let cell of cellsToCheck) {
      let cellPosition = getPositionByCell(cell);

      for (let directionLine in directions) {
        let direction = directions[directionLine]; //direction = eg. horizontal value ([[0, 1],[0, -1]])
        let toBeatInDirectionLine = [];

        for (var i = 0; i < direction.length; i++) { //i = eg. horizontal up ([0,1])
          checkNeighbour(cell, cellPosition, direction[i], cellsWithBubblesToBeat, toBeatInDirectionLine);
        }

        if (toBeatInDirectionLine.length >= 4) {
          for (let cell of toBeatInDirectionLine) {
            if (!cellsWithBubblesToBeat.includes(cell)) cellsWithBubblesToBeat.push(cell);
          }
          if (!cellsWithBubblesToBeat.includes(cell)) cellsWithBubblesToBeat.push(cell);
        }
      }
    }

    if (cellsWithBubblesToBeat.length === 0) return false;
    else return cellsWithBubblesToBeat;
  };

  let getPositionByCell = (cell) => {
    for (var y = 0; y < this.cells.length; y++)
      for (var x = 0; x < this.cells[y].length; x++)
        if (this.cells[y][x] === cell) return [x, y];
  };

  let checkNeighbour = (cell, cellPosition, coordinates, wholeDirectionsArray, directionLineArray) => { //todo: to improve
    let neighbourPosition = addCoordinates(cellPosition, coordinates);
    if (neighbourPosition) {
      let neighbourCell = getCellByPosition(neighbourPosition);
      if (neighbourCell.bubble !== null) {
        if (neighbourCell.bubble.color === cell.bubble.color) {
          if (!wholeDirectionsArray.includes(neighbourCell)) directionLineArray.push(neighbourCell);
          checkNeighbour(neighbourCell, neighbourPosition, coordinates, wholeDirectionsArray, directionLineArray);
        }
      }
    }
  };

  let getCellByPosition = (array) => {
    let x = array[0];
    let y = array[1];

    for (var i = 0; i < this.cells.length; i++)
      for (var j = 0; j < this.cells[i].length; j++)
        if (i === y && j === x) return this.cells[i][j];
  };

  let addCoordinates = (array_1, array_2) => {
    let x = array_1[0] + array_2[0];
    let y = array_1[1] + array_2[1];

    if ((x >= 0 & y >= 0) & (x <= (this.width - 1) & y <= (this.height - 1))) {
      return [x, y];
    } else {
      return false;
    }
  };


  this.beatBubbles = function(array) {
    for (let cell of array) {
      cell.removeBubble();
    }
  };

}

//####################################Cell######################################

function Cell(htmlElement, observer) {
  this.htmlElement = htmlElement;
  this.bubble = null;
  this.observer = observer;

  this.pushBubble = function(bubble) {
    this.htmlElement.appendChild(bubble.htmlElement);
    this.bubble = bubble;
  };

  this.removeAndGetBubble = function() {
    let bubble = this.bubble; //maybe more appropriate version?
    this.htmlElement.removeChild(this.bubble.htmlElement);
    this.bubble = null;
    return bubble;
  };

  this.removeBubble = function() {
    this.htmlElement.removeChild(this.bubble.htmlElement);
    this.bubble = null;
  };

  this.htmlElement.addEventListener("click", () => {
    this.observer.updateCellOnClick(this);
  });

  this.htmlElement.addEventListener("mouseover", () => {
    this.observer.updateCellAnimationOnMouseover(this);
  });

  this.htmlElement.addEventListener("mouseout", () => {
    this.observer.updateCellAnimationOnMouseout(this);
  });
}

//##################################Bubble######################################

function Bubble(destination) {
  this.htmlElement = createHtmlElement("bubble", destination);
  this.color = getRandomColor();
  this.htmlElement.style.backgroundColor = this.color;

  function getRandomColor() {
    let colors = ["#d27979", "#99336d", "#823E25", "#277552", "#2C4770", "#B84734", "#ffffb3"];
    //#ffffb3 test color
    return colors[Math.floor(Math.random() * colors.length)];
  }
}

//##################################Preview#####################################

function Preview(width) {
  this.htmlElement = createHtmlElement("preview");
  this.cells = createCellsArray(width, this);

  function createCellsArray(count, preview) {
    let previewDiv = document.getElementsByClassName("preview")[0];
    let cells = [];
    for (let i = 0; i < count; i++) {
      cells.push(new Cell(createHtmlElement("cell", previewDiv), preview));
    }
    return cells;
  }

  this.updateCellOnClick = function() {};
  this.updateCellAnimationOnMouseover = function() {};
  this.updateCellAnimationOnMouseout = function() {};


  this.addBubbles = function() {
    for (let cell of this.cells) {
      let bubble = new Bubble(cell.htmlElement);
      cell.pushBubble(bubble);
    }
  };

  this.getBubbles = function() {
    let bubbles = [];
    for (let cell of this.cells) {
      bubbles.push(cell.bubble);
    }
    return bubbles;
  };

  this.removeBubbles = function() {
    for (let cell of this.cells) {
      cell.removeBubble();
    }
  };

}

//###################################Graph######################################

function Node(id, content) {
  this.id = id;
  this.content = content;
}

function Graph(board) {
  this.nodes = createNodesFromBoard(board);
  this.adjacencyList = createAdjacencyList(this.nodes, board);

  function createNodesFromBoard(board) {
    let nodes = [];
    let id = 0;
    for (let row of board.cells) {
      for (let item of row) {
        nodes.push(new Node(id, item));
        ++id;
      }
    }
    return nodes;
  }

  function createAdjacencyList(nodes, board) {
    let adjacencyList = [];

    for (let node of nodes) { //for every node

      let neighbours = board.getAdjacentCellsOfCell(node.content); //get it's cell-neighbours
      let neighboursIDList = [];
      for (let node of nodes) { //and for every node
        for (let neighbour of neighbours) {
          if (node.content === neighbour) { //check if any (those content) is equal to neighbour; if yes - add it's id to the list
            neighboursIDList.push(node.id);
          }
        }
      }

      adjacencyList[node.id] = neighboursIDList;
    }
    return adjacencyList;
  }

  function BFS(adjList, start = 0, visited = []) {
    //visited -> list for visited nodes / 'blocked' ones
    let parent = [];
    let queue = [];

    parent[start] = null;
    queue.unshift(start);
    visited[start] = true;

    while (queue.length > 0) {
      let v = queue.shift();
      for (let neighbour of adjList[v]) {
        if (!visited[neighbour]) {
          parent[neighbour] = v;
          queue.push(neighbour);
          visited[neighbour] = true;
        }
      }
    }

    return parent;
  }

  function parentsToShortestPath(parents, from) {
    if (parents[from] !== undefined) {
      let path = [];
      while (parents[from] !== null) {
        path.push(parents[from]);
        from = parents[from];
      }
      return path;
    }
    //console.log("ERROR: 'from' index out of bounds");
  }

  this.findShortestPath = function(graph, from, to, blockedList = []) {
    let parents = BFS(graph, to, blockedList);
    let x = parentsToShortestPath(parents, from);
    return x;
  };

}

//##############################################################################
//###################################HTML#######################################

function createHtmlElement(className, destination = document.body) {
  let div = document.createElement("div");
  div.classList.add(className);
  destination.appendChild(div);
  return div;
}

function animateOnSelect(bubbleHtml, cellHtml) {
  //bubbleHtml.style.boxShadow = "3px 3px #888888"; //kinda nice option, but not such visible
  bubbleHtml.style.outline = "2px dashed " + bubbleHtml.style.backgroundColor + "";
  cellHtml.style.border = "1px dashed " + bubbleHtml.style.backgroundColor + "";
}

function unanimateOnSelect(bubbleHtml, cellHtml) {
  bubbleHtml.style.outline = "none";
  cellHtml.style.border = "1px solid white";
}

//###################################Init#######################################

function Init() {
  this.boardRow = 9;
  this.game = new Game(this.boardRow);
  this.clearAll = function() {
    this.boardRow = null;
    this.game = null;
  };
}

let init = new Init();